import React, { Component } from 'react';
import { TextInput, Button } from 'react-native-paper';
import { View } from 'react-native';


class Pretraga extends Component {
    state = {
        data: [],
        value: '',
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== null && this.state.data.length === 0) {
            this.setState({
                data: nextProps.data,
            });
        }
    }

    pretrazi = () => {
        const { data, value } = this.state;

        const filteredData = data.filter(element => {
            return this.props.criteria(element, value);
        });

        this.props.callback(filteredData);
    }

    render() {
        return (
            <View style={{ width: '90%', marginLeft: '5%' }}>
                <TextInput
                    label='Претрага'
                    value={this.state.value}
                    onChangeText={text => this.setState({ value: text })}
                />
                <Button onPress={this.pretrazi}>
                    Претражи
                </Button>
            </View>
        );
    }
}

export default Pretraga;