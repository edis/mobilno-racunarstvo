import React from 'react'
import { ScrollView, View, Modal } from 'react-native';
import { Title, Card, Paragraph, ActivityIndicator, DataTable, Text, Button } from 'react-native-paper';
import { http } from '../../util/http';
import { CheckBox } from 'react-native-elements';
import Pretraga from '../Generic/Pretraga';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'

class PrikazSvihFakulteta extends React.Component {
    state = {
        fakulteti: null,
        rukovodioci: null,
        error: null,
        dialogFakultet: false,
        dialogRukovodioci: false,
        samoAktivniRukovodioci: false,
        selektovaniFakultet: null,
    }

    static navigationOptions = {
        title: 'Факултети',
    }

    ucitajFakultete = () => {
        this.setState({
            fakulteti: null,
            rukovodioci: null,
            error: null,
            dialogFakultet: false,
            dialogRukovodioci: false,
            samoAktivniRukovodioci: false,
            selektovaniFakultet: null,
        });
        http.get('fakultet').then(resp => {
            const status = resp.data.status;
            const fakulteti = resp.data.data;
            if (status === 200) {
                this.setState({
                    fakulteti: fakulteti
                });
            } else {
                this.setError('Грешка!');
            }
        }).catch(err => this.setError(err));
    }

    setError = (err) => {
        this.setState({
            error: err,
        })
    }

    componentDidMount() {
        this.ucitajFakultete();
    }

    componentWillUnmount() {
        this.setState({
            fakultet: null,
            error: null,
        });
    }

    getPodaciOFakultetu = (fakultet) => {
        if (fakultet.podaci && fakultet.podaci.length === 0) {
            return null;
        }
        return (
            <View>
                <Title>Подаци о факултету</Title>
                <DataTable>
                    <DataTable.Header>
                        <DataTable.Title>Podatak</DataTable.Title>
                        <DataTable.Title>Vrednost</DataTable.Title>
                    </DataTable.Header>
                    {
                        fakultet.podaci.map(podatak => {
                            return (
                                <DataTable.Row key={podatak.id}>
                                    <DataTable.Cell>{podatak.tipPodatka.naziv}</DataTable.Cell>
                                    <DataTable.Cell>{podatak.vrednost}</DataTable.Cell>
                                </DataTable.Row>
                            );
                        })
                    }
                </DataTable>
            </View>
        );
    }

    prikaziDialogFakultet = (fakultet) => {
        this.setState({
            dialogFakultet: true,
            selektovaniFakultet: fakultet,
        });
    }

    zatvoriDialogFakultet = () => {
        this.setState({
            selektovaniFakultet: null,
            dialogFakultet: false,
        });
    }

    zatvoriDialogRukovodioci = () => {
        this.setState({
            rukovodioci: null,
            dialogRukovodioci: false,
            samoAktivniRukovodioci: false,
        });
    }

    getDialogFakultet = () => {
        const { dialogFakultet, selektovaniFakultet } = this.state;

        if (!dialogFakultet || !selektovaniFakultet) {
            return null;
        }

        return (
            <Modal
                visible={dialogFakultet}
                onDismiss={this.zatvoriDialogFakultet}
                onRequestClose={this.zatvoriDialogFakultet}>
                <ScrollView>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', width: '90%' }}>
                        <Title>{selektovaniFakultet.naziv}</Title>
                        <DataTable>
                            <DataTable.Header>
                                <DataTable.Title>Податак</DataTable.Title>
                                <DataTable.Title>Вредност</DataTable.Title>
                            </DataTable.Header>
                            <DataTable.Row>
                                <DataTable.Cell>Матични број</DataTable.Cell>
                                <DataTable.Cell>{selektovaniFakultet.maticniBroj}</DataTable.Cell>
                            </DataTable.Row>
                            <DataTable.Row>
                                <DataTable.Cell>Порески број</DataTable.Cell>
                                <DataTable.Cell>{selektovaniFakultet.poreskiBroj}</DataTable.Cell>
                            </DataTable.Row>
                            <DataTable.Row>
                                <DataTable.Cell>Правна форма</DataTable.Cell>
                                <DataTable.Cell>{selektovaniFakultet.pravnaForma.naziv}</DataTable.Cell>
                            </DataTable.Row>
                            <DataTable.Row>
                                <DataTable.Cell>Научна област</DataTable.Cell>
                                <DataTable.Cell>{selektovaniFakultet.naucnaOblast.naziv}</DataTable.Cell>
                            </DataTable.Row>
                            <DataTable.Row>
                                <DataTable.Cell>Врста организације</DataTable.Cell>
                                <DataTable.Cell>{selektovaniFakultet.vrstaOrganizacije.naziv}</DataTable.Cell>
                            </DataTable.Row>
                            <DataTable.Row>
                                <DataTable.Cell>Опис</DataTable.Cell>
                                <DataTable.Cell>{selektovaniFakultet.opis}</DataTable.Cell>
                            </DataTable.Row>
                        </DataTable>
                    </View>

                    {this.getPodaciOFakultetu(selektovaniFakultet)}
                    <Button onPress={this.zatvoriDialogFakultet}>Затвори приказ</Button>
                </ScrollView>
            </Modal>
        );
    }

    renderRukovodioci = (rukovodioci, samoAktivni = false) => {
        if (rukovodioci === null || rukovodioci.length === 0) {
            return <Text>Нема руководиоца</Text>;
        }

        const title = (samoAktivni) ? 'Активни руководиоци' : 'Руководиоци';

        return (
            <ScrollView>
                <Title>{title}</Title>

                <CheckBox checked={samoAktivni} title='Само активни' onPress={() => {
                    const { selektovaniFakultet } = this.state;
                    const aktivni = !samoAktivni;
                    http.get('fakultet/' + selektovaniFakultet.id + '/rukovodioci?samoAktivni=' + (aktivni ? 'true' : 'false'))
                        .then(resp => {
                            if (resp.data.status === 200) {
                                this.setState({
                                    dialogRukovodioci: true,
                                    rukovodioci: resp.data.data,
                                    samoAktivniRukovodioci: aktivni
                                })
                            } else {
                                alert(resp.data.message);
                                this.setState({
                                    rukovodioci: null,
                                    dialogRukovodioci: false,
                                    selektovaniFakultet: null,
                                    samoAktivniRukovodioci: false,
                                });
                            }
                        }).catch(err => alert(err));
                }} />


                {
                    rukovodioci.map(rukovodilac => {
                        return (
                            <DataTable style={{ marginBottom: 20 }} key={rukovodilac.ime + rukovodilac.prezime + rukovodilac.datumOd + rukovodilac.datumDo}>
                                <Title>{rukovodilac.ime + ' ' + rukovodilac.prezime}</Title>
                                <DataTable.Row>
                                    <DataTable.Cell>Датум од</DataTable.Cell>
                                    <DataTable.Cell>{rukovodilac.datumOd}</DataTable.Cell>
                                </DataTable.Row>
                                <DataTable.Row>
                                    <DataTable.Cell>Датум до</DataTable.Cell>
                                    <DataTable.Cell>{rukovodilac.datumDo}</DataTable.Cell>
                                </DataTable.Row>
                                <DataTable.Row>
                                    <DataTable.Cell>Тип руководиоца</DataTable.Cell>
                                    <DataTable.Cell>{rukovodilac.tipRukovodioca.naziv}</DataTable.Cell>
                                </DataTable.Row>
                                <DataTable.Row>
                                    <DataTable.Cell>Звање</DataTable.Cell>
                                    <DataTable.Cell>{rukovodilac.zvanje.naziv}</DataTable.Cell>
                                </DataTable.Row>
                                <DataTable.Row>
                                    <DataTable.Cell>Титула</DataTable.Cell>
                                    <DataTable.Cell>{rukovodilac.titula.naziv}</DataTable.Cell>
                                </DataTable.Row>
                            </DataTable>
                        )
                    })
                }
            </ScrollView>
        );
    }

    getDialogRukovodioci = () => {
        const { dialogRukovodioci, selektovaniFakultet, samoAktivniRukovodioci, rukovodioci } = this.state;

        if (!dialogRukovodioci || !selektovaniFakultet || !rukovodioci) {
            return null;
        }

        return (
            <Modal
                visible={dialogRukovodioci}
                onDismiss={this.zatvoriDialogRukovodioci}
                onRequestClose={this.zatvoriDialogRukovodioci}>
                <ScrollView>
                    <View>
                        {this.renderRukovodioci(rukovodioci, samoAktivniRukovodioci)}
                    </View>
                    <Button onPress={this.zatvoriDialogRukovodioci}>Затвори приказ</Button>
                </ScrollView>
            </Modal>
        );
    }

    getContent = () => {
        if (this.state.fakulteti === null) {
            return <ActivityIndicator size='large' />;
        }

        if (this.state.error !== null) {
            return (
                <Text>
                    Дошло је до грешке приликом учитавања факултета.
                </Text>
            );
        }

        return (
            this.state.fakulteti.map(fakultet => {
                return (
                    <View key={fakultet.id} style={{ margin: 10 }}>
                        <Card style={{ backgroundColor: '#f3f3f3' }}>
                            <Card.Content>
                                <Title>{fakultet.naziv}</Title>
                                <Paragraph>{fakultet.opis}</Paragraph>

                                {this.getPodaciOFakultetu(fakultet)}
                                <Button onPress={() => this.prikaziDialogFakultet(fakultet)}>
                                    Прикажи више детаља
                                </Button>

                                <Button onPress={() => {
                                    http.get('fakultet/' + fakultet.id + '/rukovodioci')
                                        .then(resp => {
                                            if (resp.data.status === 200) {
                                                this.setState({
                                                    selektovaniFakultet: fakultet,
                                                    dialogRukovodioci: true,
                                                    rukovodioci: resp.data.data,
                                                })
                                            } else {
                                                alert(resp.data.message);
                                                this.setState({
                                                    rukovodioci: null,
                                                    dialogRukovodioci: false,
                                                    selektovaniFakultet: null,
                                                });
                                            }
                                        }).catch(err => alert(err));
                                }}>
                                    Прикажи руководиоце
                                </Button>
                            </Card.Content>
                        </Card>
                    </View>
                );
            })
        );
    }

    render() {
        return (
            <ScrollView>

                <Button onPress={this.ucitajFakultete}>
                    <MaterialIcon name='reload' size={30} />
                </Button>

                <Pretraga
                    data={this.state.fakulteti}
                    criteria={(obj, text) => {
                        const kriterijum = obj.naziv + ' ' + obj.maticniBroj + ' ' + obj.poreskiBroj;
                        return text.trim() === '' ? true : kriterijum.toLowerCase().includes(text.toLowerCase());
                    }}
                    callback={(data) => this.setState({ fakulteti: data })}
                />

                {this.getContent()}

                {this.getDialogFakultet()}
                {this.getDialogRukovodioci()}
            </ScrollView>
        );
    }
}

export default PrikazSvihFakulteta;