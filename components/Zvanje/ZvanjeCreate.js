import React from 'react';
import { Alert, View } from 'react-native'
import { http } from '../../util/http';
import { Title, TextInput, Button } from 'react-native-paper'

class ZvanjeCreate extends React.Component {
    state = {
        naziv: ''
    }

    static navigationOptions = {
        title: 'Zvanje'
    }

    kreirajZvanje = () => {
        const { naziv } = this.state;
        if (naziv.trim() === '') {
            alert('Унеси назив');
            return;
        }

        const zvanje = {
            naziv: naziv
        }

        http.post('zvanje', zvanje).then(resp => {
            if (resp.data.status === 200) {
                this.setState({ naziv: '' });
                alert('Звање ' + resp.data.data.naziv + ' је креирано');
            } else {
                Alert.alert(resp.data.message);
            }
        }).catch(err => Alert.alert(err));
    }

    render() {
        return (
            <View style={{ justifyContent: "center", alignItems: 'center' }}>
                <View style={{ width: '80%' }}>
                    <Title style={{ textAlign: 'center' }}>Креирај звање</Title>
                    <TextInput
                        label='Назив'
                        value={this.state.naziv}
                        onChangeText={(val) => this.setState({ naziv: val })} />
                    <Button onPress={this.kreirajZvanje}>Креирај</Button>
                </View>
            </View>
        );
    }
}

export default ZvanjeCreate;