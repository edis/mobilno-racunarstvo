import React from 'react'
import { ScrollView, Text, View, Modal, Alert } from 'react-native';
import { http } from '../../util/http';
import { ActivityIndicator, Card, Title, Button } from 'react-native-paper'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import Pretraga from '../Generic/Pretraga';


class ZvanjeShow extends React.Component {

    static navigationOptions = {
        title: 'Звања'
    }

    state = {
        zvanja: null,
        izabranoZvanje: null,
        error: null,
        dialog: {
            visible: false,
        }
    }

    ucitajZvanja = () => {
        this.setState({
            zvanja: null,
            error: null,
            izabranoZvanje: null,
            dialog: {
                visible: false,
            }
        });
        http.get('zvanje')
            .then(resp => {
                const status = resp.data.status;
                const zvanja = resp.data.data;
                if (status === 200) {
                    this.setState({
                        zvanja: zvanja
                    });
                } else {
                    this.setError('Грешка');
                }
            })
            .catch(err => this.setError(err));
    }

    setError = (err) => {
        this.setState({ error: err });
    }

    componentDidMount() {
        this.ucitajZvanja();
    }

    obirisiZvanje = () => {
        const { izabranoZvanje } = this.state;
        http.delete('zvanje/' + izabranoZvanje.id).then(resp => {
            if (resp.data.status === 200) {
                this.ucitajZvanja();
                alert('Звање ' + resp.data.data.naziv + ' је успешно обрисано');
            } else {
                Alert.alert(resp.data.message);
            }
        }).catch(err => Alert.alert(err));
    }

    hideObrisiZvanjeDialog = () => {
        this.setState({
            dialog: {
                visible: false,
            }
        })
    }

    showObrisiZvanjeDialog = () => {
        this.setState({
            dialog: {
                visible: true,
            }
        })
    }

    getObrisiZvanjeDialog = () => {
        const { izabranoZvanje } = this.state;
        if (izabranoZvanje === null) {
            return null;
        }

        return (
            <Modal
                visible={this.state.dialog.visible}
                onDismiss={this.hideObrisiZvanjeDialog}
                onRequestClose={this.hideObrisiZvanjeDialog}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', width: '90%' }}>
                    <Title>Обриши звање {izabranoZvanje.naziv}?</Title>
                    <Button onPress={this.obirisiZvanje}><MaterialIcon name='delete' size={50} /></Button>
                    <Button onPress={this.hideObrisiZvanjeDialog}>Одустани</Button>
                </View>
            </Modal>
        );
    }

    getContent = () => {
        const { zvanja, error } = this.state;
        if (zvanja === null) {
            return (
                <ActivityIndicator size='large' />
            );
        }

        if (error !== null) {
            return (<Text>Дошло је до грешке.</Text>);
        }

        return (
            zvanja.map(zvanje => {
                return (
                    <View key={zvanje.id} style={{ margin: 10 }}>
                        <Card style={{ backgroundColor: '#f3f3f3' }}>
                            <Card.Content>
                                <Title>{zvanje.naziv}</Title>
                            </Card.Content>
                            <Card.Actions>
                                <Button onPress={() => this.props.navigation.navigate('ZvanjeEdit', {
                                    zvanje: zvanje
                                })}>
                                    <FontAwesomeIcon name='edit' size={30} />
                                </Button>
                                <Button onPress={async () => {
                                    await this.setState({ izabranoZvanje: zvanje });
                                    this.showObrisiZvanjeDialog()
                                }}>
                                    <MaterialIcon name='delete' size={30} />
                                </Button>
                            </Card.Actions>
                        </Card>
                    </View>
                );
            })
        );
    }

    render() {
        return (
            <ScrollView>
                <Button onPress={this.ucitajZvanja}>
                    <MaterialIcon name='reload' size={30} />
                </Button>

                <Pretraga
                    data={this.state.zvanja}
                    criteria={(zvanje, text) => text.trim() === '' ? true : zvanje.naziv.toLowerCase().includes(text.toLowerCase())}
                    callback={(data) => this.setState({ zvanja: data })}
                />

                {this.getContent()}

                {this.getObrisiZvanjeDialog()}

            </ScrollView>
        );
    }
}

export default ZvanjeShow;