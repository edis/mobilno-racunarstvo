import React from 'react';
import { View } from 'react-native'
import { Title, TextInput, Button } from 'react-native-paper'
import { baseAPIUri } from '../../util/baseAPIUri';
import { http } from '../../util/http';


class Login extends React.Component {

    static navigationOptions = {
        title: 'Пријави се',
    }

    state = {
        username: '',
        password: '',
    }

    login = () => {
        const url = baseAPIUri + 'auth/login';
        const user = {
            username: this.state.username,
            password: this.state.password,
        }

        http.post(url, user)
            .then(resp => {
                if (resp.data.status === 200) {
                    this.props.onLogin(resp.data.data);
                } else {
                    alert(resp.data.message);
                }
            }).catch(err => alert(err));
    }

    render() {
        return (
            <View style={{ width: '100%', marginTop: '20%', flexDirection: 'column', flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Title style={{ textAlign: 'center' }}>Пријави се</Title>
                <TextInput
                    style={{ width: '80%', height: 50, marginBottom: 20 }}
                    label='Корисничко име'
                    value={this.state.username}
                    onChangeText={(val) => this.setState({ username: val })} />

                <TextInput
                    style={{ width: '80%', height: 50 }}
                    secureTextEntry={true}
                    label='Шифра'
                    value={this.state.password}
                    onChangeText={(val) => this.setState({ password: val })} />
                <Button onPress={this.login}>Пријави се</Button>
            </View>
        );
    }
}

export default Login;