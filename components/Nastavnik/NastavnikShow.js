import React from 'react'
import { ScrollView, Text, View, Modal, FlatList } from 'react-native';
import { http } from '../../util/http';
import { ActivityIndicator, Card, Title, Button } from 'react-native-paper'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Pretraga from '../Generic/Pretraga';


class NastavnikShow extends React.Component {

    static navigationOptions = {
        title: 'Наставници'
    }

    state = {
        nastavnici: null,
        error: null,

        selektovaniNastavnik: null,

        zvanja: null,
        titule: null,

        dialogZvanje: false,
        dialogTitula: false,
        dialogPrikaziNastavnika: false,
    }

    ucitajZvanja = () => {
        http.get('zvanje')
            .then(resp => {
                const status = resp.data.status;
                const zvanja = resp.data.data;
                if (status === 200) {
                    this.setState({
                        zvanja: zvanja
                    });
                } else {
                    this.setError('Грешка');
                }
            })
            .catch(err => this.setError(err));
    }

    ucitajTitule = () => {
        http.get('titula')
            .then(resp => {
                const status = resp.data.status;
                const titule = resp.data.data;
                if (status === 200) {
                    this.setState({
                        titule: titule
                    });
                } else {
                    this.setError('Грешка');
                }
            })
            .catch(err => this.setError(err));
    }

    ucitaj = () => {
        http.get('nastavnik')
            .then(resp => {
                const status = resp.data.status;
                const nastavnici = resp.data.data;
                if (status === 200) {
                    this.setState({
                        nastavnici: nastavnici
                    });
                } else {
                    this.setError('Грешка');
                }
            })
            .catch(err => this.setError(err));
    }

    setError = (err) => {
        this.setState({ error: err });
    }

    componentDidMount() {
        this.ucitaj();
        this.ucitajZvanja();
        this.ucitajTitule();
    }

    zatvoriDialogDodajZvanje = () => {
        this.setState({
            dialogZvanje: false,
        });
    }

    zatvoriDialogDodajTitulu = () => {
        this.setState({
            dialogTitula: false,
        });
    }

    zatvoriDialogPrikaziNastavnika = () => {
        this.setState({
            dialogPrikaziNastavnika: false,
        });
    }

    dodajZvanje = (zvanje) => {
        const { selektovaniNastavnik } = this.state;

        if (zvanje && selektovaniNastavnik) {
            http.post('nastavnik/' + selektovaniNastavnik.id + '/zvanje', {
                id: zvanje.id,
                naziv: zvanje.naziv,
            })
                .then(resp => {
                    if (resp.data.status === 200) {
                        alert('Звање је додато наставнику.');
                    } else {
                        alert(resp.data.message);
                    }
                }).catch(err => alert(err));
        }
    }

    dodajTitulu = (titula) => {
        const { selektovaniNastavnik } = this.state;

        if (titula && selektovaniNastavnik) {
            http.post('nastavnik/' + selektovaniNastavnik.id + '/titula', {
                id: titula.id,
                naziv: titula.naziv,
            })
                .then(resp => {
                    if (resp.data.status === 200) {
                        alert('Титула је додата наставнику.');
                    } else {
                        alert(resp.data.message);
                    }
                }).catch(err => alert(err));
        }
    }

    getDodajZvanjeDialog = () => {
        const { zvanja, selektovaniNastavnik } = this.state;
        if (zvanja === null) {
            return null;
        }

        if (!this.state.dialogZvanje) {
            return null;
        }

        const data = zvanja.map(zvanje => {
            return {
                ...zvanje,
                key: zvanje.id + '',
            }
        })

        return (
            <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center', width: '100%' }}>
                <Modal
                    visible={this.state.dialogZvanje}
                    onDismiss={this.zatvoriDialogDodajZvanje}
                    onRequestClose={this.zatvoriDialogDodajZvanje}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>Додај звање наставнику {selektovaniNastavnik.ime + ' ' + selektovaniNastavnik.prezime}</Title>
                        <FlatList
                            data={data}
                            renderItem={({ item }) => {
                                return (
                                    <View>
                                        <Title>{item.naziv}</Title>
                                        <Button onPress={() => this.dodajZvanje(item)}>
                                            <Icon name='create' size={25} />
                                        </Button>
                                    </View>
                                )
                            }
                            } />
                        <Button onPress={this.zatvoriDialogDodajZvanje}>Одустани</Button>
                    </View>
                </Modal>
            </View>
        );
    }

    getPrikaziNastavnikaDialog = () => {
        const { selektovaniNastavnik } = this.state;
        if (selektovaniNastavnik === null) {
            return null;
        }

        if (!this.state.dialogPrikaziNastavnika) {
            return null;
        }

        return (
            <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center', width: '100%' }}>
                <Modal
                    visible={this.state.dialogPrikaziNastavnika}
                    onDismiss={this.zatvoriDialogPrikaziNastavnika}
                    onRequestClose={this.zatvoriDialogPrikaziNastavnika}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>{this.getNastavnikValue(selektovaniNastavnik)}</Title>
                        <Title>Звања</Title>
                        {
                            selektovaniNastavnik.zvanja.map(zvanje => {
                                return (
                                    <Text key={zvanje.id}>
                                        {zvanje.naziv} од {zvanje.datumOd}
                                    </Text>
                                );
                            })
                        }

                        <Title>Титуле</Title>
                        {
                            selektovaniNastavnik.titule.map(titula => {
                                return (
                                    <Text key={titula.id}>
                                        {titula.naziv} од {titula.datumOd}
                                    </Text>
                                );
                            })
                        }
                        <Button onPress={this.zatvoriDialogPrikaziNastavnika}>Затвори</Button>
                    </View>
                </Modal>
            </View>
        );
    }

    getDodajTituluDialog = () => {
        const { titule, selektovaniNastavnik } = this.state;

        if (titule === null) {
            return null;
        }

        if (!this.state.dialogTitula) {
            return null;
        }

        const data = titule.map(titula => {
            return {
                ...titula,
                key: titula.id + '',
            }
        })

        return (
            <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center', width: '100%' }}>
                <Modal
                    visible={this.state.dialogTitula}
                    onDismiss={this.zatvoriDialogDodajTitulu}
                    onRequestClose={this.zatvoriDialogDodajTitulu}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>Додај титулу наставнику {selektovaniNastavnik.ime + ' ' + selektovaniNastavnik.prezime}</Title>
                        <FlatList
                            data={data}
                            renderItem={({ item }) => {
                                return (
                                    <View>
                                        <Title>{item.naziv}</Title>
                                        <Button onPress={() => this.dodajTitulu(item)}>
                                            <Icon name='create' size={25} />
                                        </Button>
                                    </View>
                                )
                            }
                            }
                        />
                        <Button onPress={this.zatvoriDialogDodajTitulu}>Одустани</Button>
                    </View>
                </Modal>
            </View>
        );
    }

    getNastavnikValue = (nastavnik) => {
        const titula = nastavnik.titule != null && nastavnik.titule.length > 0 ? nastavnik.titule[0].naziv : '';
        const zvanje = nastavnik.zvanja != null && nastavnik.zvanja.length > 0 ? nastavnik.zvanja[0].naziv : '';
        const imePrezime = nastavnik.ime + ' ' + nastavnik.prezime;

        return zvanje + ' ' + titula + ' ' + imePrezime;
    }

    getContent = () => {
        const { nastavnici, error } = this.state;
        if (nastavnici === null) {
            return (
                <ActivityIndicator size='large' />
            );
        }

        if (error !== null) {
            return (<Text>Дошло је до грешке.</Text>);
        }

        return (
            nastavnici.map(nastavnik => {
                return (
                    <View key={nastavnik.id} style={{ margin: 10 }}>
                        <Card style={{ backgroundColor: '#f3f3f3' }}>
                            <Card.Content>
                                <Title>{this.getNastavnikValue(nastavnik)}</Title>
                            </Card.Content>
                            <Card.Actions>
                                <Button onPress={() => this.setState({ dialogPrikaziNastavnika: true, selektovaniNastavnik: nastavnik })}>
                                    Прикажи
                                </Button>

                                <Button onPress={() => this.setState({ dialogZvanje: true, selektovaniNastavnik: nastavnik })}>
                                    Додај звање
                                </Button>

                                <Button onPress={() => this.setState({ dialogTitula: true, selektovaniNastavnik: nastavnik })}>
                                    Додај титулу
                                </Button>
                            </Card.Actions>
                        </Card>
                    </View>
                );
            })
        );
    }

    render() {
        return (
            <ScrollView>
                <Button onPress={this.ucitaj}>
                    <MaterialIcon name='reload' size={30} />
                </Button>

                <Pretraga
                    data={this.state.nastavnici}
                    criteria={(obj, text) => {
                        const imePrezime = obj.ime.toLowerCase() + ' ' + obj.prezime.toLowerCase();
                        return text.trim() === '' ? true : imePrezime.toLocaleLowerCase().includes(text.toLowerCase());
                    }}
                    callback={(data) => this.setState({ nastavnici: data })}
                />

                {this.getContent()}

                {this.getPrikaziNastavnikaDialog()}
                {this.getDodajZvanjeDialog()}
                {this.getDodajTituluDialog()}

            </ScrollView>
        );
    }
}

export default NastavnikShow;