import React from 'react';
import { View, Text } from 'react-native'

class Home extends React.Component {

    static navigationOptions = {
        title: 'Почетна страница',
    }

    render() {
        return (
            <View style={{marginTop: '40%', justifyContent: 'center', alignItems: 'center'}}>
                <Text>
                    Добро дошли
                </Text>
            </View>
        );
    }
}

export default Home;