import React from 'react'
import { ScrollView, Text, View } from 'react-native';
import { http } from '../../util/http';
import { ActivityIndicator, Card, Title, Button } from 'react-native-paper'

class NaucnaOblastShow extends React.Component {

    static navigationOptions = {
        title: 'Научне области'
    }

    state = {
        naucneOblasti: null,
        error: null,
    }

    ucitajNaucneOblasti = () => {
        http.get('naucna-oblast')
            .then(resp => {
                const status = resp.data.status;
                const naucneOblasti = resp.data.data;
                if (status === 200) {
                    this.setState({
                        naucneOblasti: naucneOblasti
                    });
                } else {
                    this.setError('Грешка');
                }
            })
            .catch(err => this.setError(err));
    }

    setError = (err) => {
        this.setState({ error: err });
    }

    componentDidMount() {
        this.ucitajNaucneOblasti();
    }

    getContent = () => {
        const { naucneOblasti, error } = this.state;
        if (naucneOblasti === null) {
            return (
                <ActivityIndicator size='large' />
            );
        }

        if (error !== null) {
            return (<Text>Дошло је до грешке.</Text>);
        }

        return (
            naucneOblasti.map(naucnaOblast => {
                return (
                    <View key={naucnaOblast.id} style={{ margin: 10 }}>
                        <Card style={{ backgroundColor: '#f3f3f3' }}>
                            <Card.Content>
                                <Title>{naucnaOblast.naziv}</Title>
                            </Card.Content>
                            <Card.Actions>
                            </Card.Actions>
                        </Card>
                    </View>
                );
            })
        );
    }

    render() {
        return (
            <ScrollView>
                {this.getContent()}
            </ScrollView>
        );
    }
}

export default NaucnaOblastShow;