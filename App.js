import React from 'react';
import { createStackNavigator, createDrawerNavigator, createAppContainer, createMaterialTopTabNavigator } from 'react-navigation'
import PrikazSvihFakulteta from './components/Fakultet/FakultetShow';
import Home from './components/Home/Home';
import NaucnaOblastShow from './components/NaucnaOblast/NaucnaOblastShow';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
import ZvanjeShow from './components/Zvanje/ZvanjeShow';
import ZvanjeCreate from './components/Zvanje/ZvanjeCreate';
import ZvanjeEdit from './components/Zvanje/ZvanjeEdit';
import Login from './components/Login/Login';
import NastavnikShow from './components/Nastavnik/NastavnikShow';

export default class App extends React.Component {

    state = {
        user: null,
    }

    componentWillMount() {
        console.log('unmount');
        this.setState({
            user: null,
        });
    }

    componentWillUnmount() {
        console.log('unmount');
        this.setState({
            user: null,
        });
    }

    login = (user) => {
        if (user !== null) {
            this.setState({
                user
            });
        }
    }

    render() {

        let component = <Login onLogin={this.login} />;

        if (this.state.user !== null) {
            component = <AppContainer />;
        }

        return (
            component
        );
    }
}

const settings = {
    unmountInactiveRoutes: true,
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: 'gray'
        },
    },
};

const StackHome = createStackNavigator({
    Home: Home
}, settings);

const StackFakulteti = createStackNavigator({
    Fakulteti: PrikazSvihFakulteta
}, settings);

const TabZvanja = createMaterialTopTabNavigator({
    Zvanja: {
        screen: ZvanjeShow,
        navigationOptions: {
            tabBarIcon: () => <Icon name='eye' size={25} />
        }
    },
    KreirajZvanje: {
        screen: ZvanjeCreate,
        navigationOptions: {
            tabBarIcon: () => <MaterialIcon name='create' size={25} />
        }
    }
}, {
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
        },
    });

const StackNaucneOblasti = createStackNavigator({
    PrikazNaucneOblasti: NaucnaOblastShow
}, settings);

const StackZvanja = createStackNavigator({
    PrikazZvanja: TabZvanja,
    ZvanjeEdit: ZvanjeEdit
}, settings);

const StackNastavnik = createStackNavigator({
    PrikazNastavnika: NastavnikShow
}, settings);

const AppDrawerNavigator = createDrawerNavigator({
    Home: StackHome,
    Fakulteti: StackFakulteti,
    Zvanja: StackZvanja,
    NaucneOblasti: StackNaucneOblasti,
    Nastavnici: StackNastavnik,
}, {
        initialRouteName: 'Home',
        unmountInactiveRoutes: true,
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'gray',
            },
        },
    });

const AppContainer = createAppContainer(AppDrawerNavigator);
