import { baseAPIUri } from './baseAPIUri'
import axios from 'axios'

export const http = axios.create({
    baseURL: baseAPIUri,
    timeout: 2000,
});